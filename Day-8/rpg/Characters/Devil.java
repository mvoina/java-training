package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */

public class Devil extends Villain{

    public Devil(String name) {
        super(name);
        this.setHealth(1900);
        setLevel(4);
    }

    public Villain newDevil(){
        Devil newDevil = new Devil("Devil");
        return newDevil;
    }
}
