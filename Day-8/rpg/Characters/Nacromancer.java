package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Nacromancer extends Villain{

    public Nacromancer(String name) {
        super(name);
        this.setHealth(1550);
        setLevel(3);
    }

    public Villain newNacromancer(){
        Nacromancer newNacromancer = new Nacromancer("Nacromancer");
        return newNacromancer;
    }
}
