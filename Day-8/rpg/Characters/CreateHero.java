package rpg.Characters;

/**
 * Created by aniov on 3/1/2016.
 */
import java.lang.Class;
//Using Singleton pattern - can create only 1 object of this class during the life of the pogramm
public class CreateHero{

        private static Hero newHero;

        private CreateHero(){}

        public static Hero getNewHero(String heroType, String currentPlayer, String currentHeroName, int currentLevel) throws ClassNotFoundException {

            if (newHero == null){
                try {
                    newHero = (Hero) Class.forName("rpg.Characters." + heroType).newInstance();

                    newHero.setPlayerName(currentPlayer);
                    newHero.setCharName(currentHeroName);
                    newHero.setLevel(currentLevel);
                    newHero.setHealth(newHero.getHealth() * currentLevel);
                    //  myHero.setArmour();
                    newHero.setAttackpower(newHero.getAttackPower());
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return newHero;
        }

//        public static void heroIsDead(){
//            newHero = null;
 //       }
    }
