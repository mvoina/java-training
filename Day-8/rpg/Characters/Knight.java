package rpg.Characters;

import rpg.Artifacts.Sword;

/**
 * Created by aniov on 2/23/2016.
 */
public class Knight extends Hero{

    public Knight() {
//       Sword sword = new Sword(3,5);
//        this.setHealth(130 + sword.getArtifactHealth());
//        setLevel(1);

        setHealth(3000);
        setAttackpower(25);
    }

    public static Hero newKnight(){
        Knight newKnight = new Knight();
        return newKnight;
    }
}
