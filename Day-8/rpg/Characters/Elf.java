package rpg.Characters;

import rpg.Artifacts.Bow;

/**
 * Created by aniov on 2/23/2016.
 */
public class Elf extends Hero{

    public Elf() {
//        new Bow(0,0);
//        this.setHealth(80 + bow.getArtifactHealth());
 //       setLevel(1);

        setHealth(1500);
        setAttackpower(32);
    }

    public static Hero newElf(){
        Elf newElf = new Elf();
        return newElf;
    }
}
