package rpg.Characters;

import com.sun.org.apache.xerces.internal.xs.StringList;
import rpg.Artifacts.Artifact;
import rpg.Artifacts.*;
import rpg.FileHandle.FileHandle;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by aniov on 2/23/2016.
 */

abstract public class Hero extends Character{

        private String playerName;
//        public StringList playerChars;
        private String charName;
        private float armour;
        private float attackpower;

    public Hero(){
//        FileHandle t = new FileHandle();
//        setPlayerName(t.readPlayerNameFromInput());
//        setCharName(t.readPlayerNameFromInput());
        setPlayerName(playerName);
        setCharName(charName);
        setAlive(true);
        setArmour(2);
        setAttackpower(2);
        System.out.println("ID for " + getPlayerName() + " is: " + id);
    }
    public String getPlayerName(){
        return playerName;
    }
    public String getCharName(){
        return getCharName();
    }
    public float getArmour(){
        return armour;
    }
    public float getAttackPower(){
        return attackpower;
    }
    public void setPlayerName(String playerName){
        this.playerName = playerName;
    }
    public void setCharName(String charName){
        this.playerName = playerName;
    }
    public  void setArmour(float armour){
        this.armour = armour;
    }
    public void setAttackpower(float attackpower){
        this.attackpower = attackpower;
    }
    public float calcAttackPowerHero(){

        float armour = getArmour();
        float atackpower = getAttackPower();
        return (getLevel() * armour * atackpower);
    }
}
