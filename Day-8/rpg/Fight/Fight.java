package rpg.Fight;

/**
 * Created by aniov on 2/23/2016.
 */

import rpg.Characters.Hero;
import rpg.Characters.Villain;

public class Fight {

    public static void fightUntilDead(Hero myHero, Villain myVillian){

        while (myHero.gettAlive() && myVillian.gettAlive()){

            printHealth(myHero, myVillian);
            attack(myHero, myVillian);
            System.out.println(myHero.getHealth() + " " + myVillian.getHealth());
        }
    }

    static void attack(Hero myHero, Villain myVillian) {

        float myHeroAttack = myHero.calcAttackPowerHero();
        float myVillianAttack = myVillian.calcAttackPowerVillain();

        if (myHero.gettAlive()){
            myHero.setHealth(myHero.getHealth() - (int)(myVillianAttack + myVillianAttack));// / myHero.getArmour()));
        }
        if (myVillian.gettAlive()){
            myVillian.setHealth(myVillian.getHealth() - (int)myHeroAttack);
        }
        setAlive(myHero, myVillian);
        printFight(myHero, myVillian);
    }

    public static void setAlive(Hero myHero, Villain myVillian){

        if (myHero.getHealth() <= 0)
            myHero.setAlive(false);
        if (myVillian.getHealth() <= 0)
            myVillian.setAlive(false);
    }

    public static void printFight(Hero myHero, Villain myVillian){

        if (myHero.gettAlive() == false) {
            System.out.println("OMG, OMG, Hero is dead\n");
        }
        if (myVillian.gettAlive() == false)
            System.out.println("The Monster is finaly dead\n");
    }

    public static void  printHealth(Hero myHero, Villain myVillian){

        System.out.println("My Hero: " + myHero.getPlayerName() + " has " + myHero.getHealth() + " health");
  //      System.out.println("Villian: " + myVillian.getName() + " has " + myVillian.getHealth() + " health");
    }


}
