package rpg;

import rpg.Characters.*;
import rpg.FileHandle.FileHandle;
import rpg.Mission.Mission;
import rpg.graph.Graphic;

import java.util.ArrayList;
import java.util.List;
import java.lang.Class;

/**
 * Created by Marius on 2/26/2016.
 */
public class StartGame {

    public Graphic graph = new Graphic();


    public StartGame() {

        graph.Gmain();
    }

    public static Hero startMission(String currentPlayer, String currentHeroName, String currentHeroType, int currentLevel) throws ClassNotFoundException {

        Hero myHero;
        myHero = CreateHero.getNewHero(currentHeroType, currentPlayer, currentHeroName, currentLevel);
        if (myHero.gettAlive()){
            System.out.println("Let's PLAY " + currentPlayer + " with <" + currentHeroName + "> with lvl " + currentLevel + " because it's a <" + currentHeroType + ">");
            Mission.missionOne(myHero);
            return myHero;
        }
        else{
            System.out.println("Your hero " + currentPlayer + " It's dead....\nPress Exit");
 //           CreateHero.heroIsDead();
            return null;
        }
    }

    public static List<String> getPlayersName(){
        FileHandle fHandle = new FileHandle();
        return fHandle.readPlayerNameFromFile();
    }
    public static List<String> getPlayerHeroesName(String playerName){
        FileHandle fHandle = new FileHandle();

        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        List<String> playerCharNames = new ArrayList<String>();
        for(FileHandle.player temp : playerStuff){
            playerCharNames.add(temp.getPlayerHeroName());
        }
        return  playerCharNames;
    }

    public static String getPlayerHeroType(String playerName, String playerHeroName){
        FileHandle fHandle = new FileHandle();
        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        for(FileHandle.player temp : playerStuff){
            if (temp.getPlayerHeroName().equals(playerHeroName)) {
                return temp.getPlayerHeroType();
            }
        }
        return  null;
    }

    public static ArrayList getPlayersHeroArtifacts(String playerName, String playerHeroName){
        FileHandle fHandle = new FileHandle();
        ArrayList artiListWithStatus = new ArrayList();
        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        List<String> tmp = new ArrayList<>();
        for(FileHandle.player temp : playerStuff) {
            if (temp.getPlayerHeroName().equals(playerHeroName)) {
                tmp = temp.getPlayerHeroArtifactList();
            }
        }
        for(int i = 0; i < tmp.size(); i++) {
            if (fHandle.readArtifacts(tmp.get(i))) {
                artiListWithStatus.add(tmp.get(i) + " " + fHandle.getArtifactHealth() + " " + fHandle.getArtifactPower() + " ");
              }
        }
       return artiListWithStatus;
    }

    public static int getHerolevel(String playerName, String playerHeroName){
        FileHandle fHandle = new FileHandle();
        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        for(FileHandle.player temp : playerStuff){
            if (temp.getPlayerHeroName().equals(playerHeroName)) {System.out.println(temp.getLevel());
                return temp.getLevel();
            }
        }
        return 0;
    }
/*
    public static int getHeroHealth(String playerName, String playerHeroName){
        FileHandle fHandle = new FileHandle();
        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        for(FileHandle.player temp : playerStuff){
            if (temp.getPlayerHeroName().equals(playerHeroName)) {System.out.println(temp.getLevel());
                return temp.;
    }*/
}
