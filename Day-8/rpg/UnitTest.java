package rpg;

import org.junit.Assert;
import org.junit.runners.model.TestClass;
import rpg.Characters.Hero;
import rpg.Characters.Nacromancer;
import rpg.Characters.Orc;
import org.junit.Test;
import rpg.Characters.Villain;
import rpg.Fight.Fight;

/**
 * Created by aniov on 3/2/2016.
 */
public class UnitTest {

    @Test
    public void testOne(){

        Hero myOrc = new Orc();
        Assert.assertEquals(myOrc.getHealth(), 2000);
    }

    @Test
    public void testDmg(){

        Villain myVillain = new Nacromancer("testVillain");
        Assert.assertEquals(myVillain.getName(), "testVillain");
    }

    @Test
    public void testAlive(){

        Villain myVillain = new Nacromancer("testVillain");
        Hero myOrc = new Orc();
        Fight.fightUntilDead(myOrc, myVillain);
        assert (myOrc.getHealth() <= 0 );
    }
}
