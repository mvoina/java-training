/**
 * Created by aniov on 2/22/2016.
 */
public class QuickSort {

    static void quicksort(String[] str, int left, int right){
        int pivot, l=left, r=right;
        pivot = (left + right)/2;
        while (l <= r){
            while (l < pivot && str[l].compareTo(str[pivot]) < 0)
                l++;
            while (r > pivot && str[r].compareTo(str[pivot]) > 0)
                r--;
            if (l <= r) {
                exchangeStrings(str, l, r);
                l++;
                r--;
            }
            if (left < r)
                quicksort(str, left, r);
            if (l < right)
                quicksort(str, l, right);
        }
    }
    static void exchangeStrings(String[] str, int ind1, int ind2){
        String temp;
        temp = str[ind1];
        str[ind1] = str[ind2];
        str[ind2] = temp;
    }
    public static void main(String[] args){
        String[] Names = {"CCC","AAA", "XXX", "ABB","ZZZ", "AAA","EEE","QQQ","AAA","MMM","JJJ","DDD", "DDD", "QQQ" };
        System.out.println("Unsorted Names:");
        for (String str:Names)
            System.out.println(str);
        int right = Names.length - 1;
        quicksort(Names, 0, right);
        System.out.println("Sorted Names:");
        for (String str:Names)
            System.out.println(str);
    }
}
