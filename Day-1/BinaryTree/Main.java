/**
 * Created by aniov on 2/22/2016.
 */
public class Main {

    public static void main(String[] args) {

        BinaryTree tree = new BinaryTree();
//Create Binary tree
        tree.addNode(19);
        tree.addNode(20);
        tree.addNode(10);
        tree.addNode(90);
        tree.addNode(95);
        tree.addNode(11);
        tree.addNode(34);
        tree.addNode(21);
//Print binary tree as: PreOrder / InOrder / PostOrder
        tree.Traverse();
//Delete Node with given value
        tree.deleteNode(34);
        tree.Traverse();
//Delete Node with given value
        tree.deleteNode(10);
        tree.Traverse();
    }
}
