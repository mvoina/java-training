/**
 * Created by aniov on 2/22/2016.
 */
public class Node {
    int data;
    Node right;
    Node left;

    public Node (int data){

        this.data = data;
    }
}