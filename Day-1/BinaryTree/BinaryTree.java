/**
 * Created by aniov on 2/22/2016.
 */
public class BinaryTree {

    Node root;

    public void addNode(int data) {

        Node nodeToAdd = new Node(data);
        if (root == null) {
            root = nodeToAdd;
            return;
        }
        addNodeToTree(root, nodeToAdd);
    }

    private void addNodeToTree(Node node, Node nodeToAdd) {

        if (nodeToAdd.data < node.data) {
            if (node.left == null)
                node.left = nodeToAdd;
            else
                addNodeToTree(node.left, nodeToAdd);
        }
        else if (nodeToAdd.data > node.data) {
            if (node.right == null)
                node.right = nodeToAdd;
            else
                addNodeToTree(node.right, nodeToAdd);
        }
    }

    public void Traverse(){
        if (root == null)
            return;
        System.out.println("\nPre Order Traverse:");
        preOrderTraverse(root);
        System.out.println("\nIn Order Traverse:");
        inOrderTraverse(root);
        System.out.println("\nPost Order Traverse:");
        postOrderTraverse(root);
    }
    // root - left - right
    private void preOrderTraverse (Node node){
        if (node == null)
            return ;
        System.out.print(node.data + " ");
        preOrderTraverse(node.left);
        preOrderTraverse(node.right);
    }
    // left - root - right
    private void inOrderTraverse (Node node){
        if (node == null)
            return ;
        inOrderTraverse(node.left);
        System.out.print(node.data + " ");
        inOrderTraverse(node.right);
    }
    // left - right - root
    private void postOrderTraverse (Node node){
        if (node == null)
            return ;
        postOrderTraverse(node.left);
        postOrderTraverse(node.right);
        System.out.print(node.data + " ");
    }

    private Node findMin(Node node){
        Node tmp = node;
        while(tmp.left != null)
            tmp = node.left;
        return tmp;
    }

    public void deleteNode(int data){
        deleteNod(root, data);
        System.out.println("\nNode with value " + data + " has been deleted");
    }

    private Node deleteNod(Node node, int data){
        if (node == null)
            return node;
        else if (data < node.data)
            node.left = deleteNod(node.left, data);
        else if (data > node.data)
            node.right = deleteNod(node.right, data);
        else{
            if (node.left == null && node.right == null)
                node = null;
            else if (node.left == null)
                node = node.right;
            else if (node.right == null)
                node = node.left;
            else{
                Node temp = findMin(node.right);
                node.data = temp.data;
                node.right = deleteNod(node.right, temp.data);
            }
        }
        return node;
    }
}