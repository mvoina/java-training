package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Mage extends Hero{

    public Mage(String name) {
        super(name);
        health = 99;
        level = 1;
    }
}
