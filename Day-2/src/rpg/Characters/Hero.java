package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */

import rpg.Artifacts.Axe;
import rpg.Artifacts.Artifact;

public class Hero extends Character{

        float armour;
        float attackpower;
        String name;

    public Hero(String name) {
        this.name = name;
        setAlive(true);
        setArmour(2);
        setAttackpower(2);


    }
    public float getArmour(){
        return armour;
    }
    public float getAttackPower(){
        return attackpower;
    }
    public String getName(){
        return name;
    }
    public  void setArmour(float armour) {
        this.armour = armour;
    }
    public void setAttackpower(float attackpower) {
        this.attackpower = attackpower;
    }
    public float calcAttackPowerHero(){

        float ar = getArmour();
        float atp = getAttackPower();
//        int artif = Artifact.getAxePower();
        return (level * ar * atp);
    }
}
