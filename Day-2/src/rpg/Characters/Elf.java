package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Elf extends Hero{

    public Elf(String name) {
        super(name);
        health = 80;
        level = 1;
    }
}
