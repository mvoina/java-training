package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Knight extends Hero{

    public Knight(String name) {
        super(name);
        health = 130;
        level = 1;
    }
}
