package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Character {

    public int health;
    public int level;
    public boolean alive;

    public int getHealth(){
        return health;
    }
    public int getLevel(){
        return level;
    }
    public boolean gettAlive() {
        return alive;
    }
    public void setHealth(int health){
        this.health = health;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

}
