package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Goblin extends Villain{

    public Goblin(String name) {
        super(name);
        health = 110;
        level = 1;
    }
}
