package rpg.Characters;


import rpg.Characters.Hero;
import rpg.Artifacts.Artifact;
import rpg.Artifacts.Axe;

/**
 * Created by aniov on 2/23/2016.
 */

public class Orc extends Hero {

    public Orc(String name) {
        super(name);
        health = 150;
        level = 2;
    }

    Axe orcAxe = new Axe();
}
