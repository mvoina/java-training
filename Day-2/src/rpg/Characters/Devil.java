package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */

public class Devil extends Villain{

    public Devil(String name) {
        super(name);
        health = 100;
        level = 1;
    }
}
