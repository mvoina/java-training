package rpg.Characters;

import rpg.Characters.Character;

/**
 * Created by aniov on 2/23/2016.
 */


public class Villain extends Character{

    String name;
    float attackpower;

    public Villain(String name) {
        this.name = name;
        setAlive(true);
        setAttackpower(1);
    }
    public String getName(){
        return name;
    }
    public float getAttackPower() {
        return attackpower;
    }
    public void setAttackpower(float attackpower) {
        this.attackpower = attackpower;
    }
    public float calcAttackPowerVillain(){

        float atp = getAttackPower();
        return (level * atp);
    }
}
