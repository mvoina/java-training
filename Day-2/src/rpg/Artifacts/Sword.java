package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
public class Sword extends Artifact{

    public Sword() {
        swordPower = 4;
        swordHealth = 15;
    }
}
