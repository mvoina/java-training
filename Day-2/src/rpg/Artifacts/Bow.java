package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
public class Bow extends Artifact {

    public Bow() {
        bowPower = 3;
        bowHealth = 9;
    }
}
