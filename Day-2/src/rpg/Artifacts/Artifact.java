package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
public class Artifact {

    int axePower;
    int axeHealth;
    int bowPower;
    int bowHealth;
    int helmPower;
    int helmHealth;
    int swordPower;
    int swordHealth;

    public int getAxePower(){
        return axePower;
    }
    public int getAxeHealth(){
        return axeHealth;
    }
    public int getBowPower(){
        return bowPower;
    }
    public int getBowHealth(){
        return bowHealth;
    }
    public int getHelmPower(){
        return helmPower;
    }
    public int getHelmHealth(){
        return helmHealth;
    }
    public int getSwordPower(){
        return swordPower;
    }
    public int getSwordHealth(){
        return swordHealth;
    }
}
