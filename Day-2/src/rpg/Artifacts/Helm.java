package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
public class Helm extends Artifact {

    public Helm() {
        helmPower = 12;
        helmHealth = 32;
    }
}
