package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
public class Axe extends Artifact{

    public Axe(){
        axePower = 2;
        axeHealth = 10;
    }
}
