/**
 * Created by aniov on 2/23/2016.
 */

import rpg.Characters.Devil;
import rpg.Characters.Mage;
import rpg.Characters.Nacromancer;
import rpg.Characters.Orc;
import rpg.Fight.Fight;


public class MainRPG {
    static public void  main(String[] args){

        Orc hero1 = new Orc("Arthas");
        Devil monster1 = new Devil("BillyBoy");
        Fight.fightUntilDead(hero1, monster1);

        Mage hero2 = new Mage("Hocus");
        Nacromancer monster2 = new Nacromancer("Spells");
        Fight.fightUntilDead(hero2, monster2);
    }
}
