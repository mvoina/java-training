package rpg.Characters;


/**
 * Created by aniov on 2/23/2016.
 */
abstract public class Character {

    public String name;
    public int health;
    public int level;
    static int idGen = 0;
    public int id = ++idGen;
    public boolean alive;

    public int getHealth(){
        return health;
    }
    public int getLevel(){
        return level;
    }
    public boolean gettAlive() {
        return alive;
    }
    public void setHealth(int health){
        this.health = health;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Character character = (Character) o;
        return id == character.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
