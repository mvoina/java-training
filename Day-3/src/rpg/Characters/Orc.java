package rpg.Characters;

import rpg.Artifacts.Helm;
import rpg.Artifacts.Axe;

/**
 * Created by aniov on 2/23/2016.
 */

public class Orc extends Hero {

    public Orc(String name) {
        super(name);
        Axe axe = new Axe(2, 4);
        new Helm(1, 3);
        health = 150 + axe.getArtifactHealth();
        level = 2;
    }
}
