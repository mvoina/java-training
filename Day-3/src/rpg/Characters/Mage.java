package rpg.Characters;

import rpg.Artifacts.Helm;

/**
 * Created by aniov on 2/23/2016.
 */
public class Mage extends Hero{

    public Mage(String name) {
        super(name);
        Helm helm = new Helm(4,3);
        health = 99 + helm.getArtifactHealth();
        level = 1;
    }
}
