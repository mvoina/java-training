package rpg.Characters;

import rpg.Artifacts.Sword;

/**
 * Created by aniov on 2/23/2016.
 */
public class Knight extends Hero{

    public Knight(String name) {
        super(name);
        Sword sword = new Sword(3,5);
        health = 130 + sword.getArtifactHealth();
        level = 1;
    }
}
