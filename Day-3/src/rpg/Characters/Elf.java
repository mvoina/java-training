package rpg.Characters;

import rpg.Artifacts.Bow;

/**
 * Created by aniov on 2/23/2016.
 */
public class Elf extends Hero{

    public Elf(String name) {
        super(name);
        Bow bow = new Bow(2,1);
        health = 80 + bow.getArtifactHealth();
        level = 1;
    }
}
