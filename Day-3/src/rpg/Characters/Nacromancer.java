package rpg.Characters;

/**
 * Created by aniov on 2/23/2016.
 */
public class Nacromancer extends Villain{

    public Nacromancer(String name) {
        super(name);
        health = 115;
        level = 3;
    }
}
