package rpg.Characters;

import rpg.Artifacts.Artifact;
import rpg.Artifacts.*;

/**
 * Created by aniov on 2/23/2016.
 */

abstract public class Hero extends Character{

        float armour;
        float attackpower;

    public Hero(String name){
        this.name = name;
        setAlive(true);
        setArmour(2);
        setAttackpower(2);
        System.out.println("ID for " + getName() + " is: " + id);
    }
    public float getArmour(){
        return armour;
    }
    public float getAttackPower(){
        return attackpower;
    }
    public String getName(){
        return name;
    }
    public  void setArmour(float armour){
        this.armour = armour;
    }
    public void setAttackpower(float attackpower){
        this.attackpower = attackpower;
    }
    public float calcAttackPowerHero(){

        float armour = getArmour();
        float atackpower = getAttackPower();
        return (level * armour * atackpower);
    }
}
