package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
abstract public class Artifact {

    int axePower = 0;
    int axeHealth= 0;
    int bowPower= 0;
    int bowHealth= 0;
    int helmPower= 0;
    int helmHealth= 0;
    int swordPower= 0;
    int swordHealth= 0;

    public int getAxePower(){
        return axePower;
    }
    public int getAxeHealth(){
        return axeHealth;
    }
    public int getBowPower(){
        return bowPower;
    }
    public int getBowHealth(){
        return bowHealth;
    }
    public int getHelmPower(){
        return helmPower;
    }
    public int getHelmHealth(){
        return helmHealth;
    }
    public int getSwordPower(){
        return swordPower;
    }
    public int getSwordHealth(){
        return swordHealth;
    }
    public int getArtifactPower(){
        return (axePower + bowPower + helmPower + swordPower);
    }
    public int getArtifactHealth(){
        return (axeHealth + bowHealth + helmHealth + swordHealth);
    }
}
