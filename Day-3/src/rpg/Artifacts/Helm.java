package rpg.Artifacts;

/**
 * Created by aniov on 2/23/2016.
 */
public class Helm extends Artifact {

    public Helm(int helmPower, int helmHealth) {
        this.helmPower = helmPower;
        this.helmHealth = helmHealth;
    }
}
