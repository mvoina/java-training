/**
 * Created by aniov on 2/23/2016.
 */

import rpg.Characters.Orc;
import rpg.Fight.Fight;
import rpg.Mission.Mission;


public class MainRPG {
    static public void  main(String[] args){
/*
        Orc hero1 = new Orc("Arthas");
        Devil monster1 = new Devil("BillyBoy");
        Fight.fightUntilDead(hero1, monster1);

        Mage hero2 = new Mage("Hocus");
        Nacromancer monster2 = new Nacromancer("Spells");
        Fight.fightUntilDead(hero2, monster2);
                //Test Overwrite <equals>
        System.out.println(hero1.equals(hero1));
*/
        Orc hero1 = new Orc("Arthas");

        Mission one = new Mission();
        one.missionOne(hero1);

        Mission two = new Mission();
        two.missionTwo(hero1);

    }
}
