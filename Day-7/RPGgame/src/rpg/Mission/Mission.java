package rpg.Mission;

import rpg.Characters.*;
import rpg.Fight.Fight;

/**
 * Created by aniov on 2/24/2016.
 */

public class Mission {

    public static void missionOne(Hero myHero){

        if (myHero.gettAlive())
            System.out.println("Mission ONE\nFight one begins.......\n");
        else
            return;
        Fight.fightUntilDead(myHero,  new Devil("The Devil"));
        if (myHero.gettAlive())
            System.out.println("End of fight one\n");
        else
            return;
        if (myHero.gettAlive())
            System.out.println("Mission ONE\nFight two begins.......\n");
        Fight.fightUntilDead(myHero, new Goblin("The Goblin"));
        if (myHero.gettAlive())
            System.out.println("End of fight two\n");
        else
            return;
        if (myHero.gettAlive())
            System.out.println("Mission ONE\nFight three begins.......\n");
        else
            return;
        Fight.fightUntilDead(myHero, new Nacromancer("The Nacromancer"));
        if (myHero.gettAlive())
            System.out.println("End of fight three and end of Mission ONE\n");
    }

    public static void missionTwo(Hero myHero){

        if (myHero.gettAlive())
            System.out.println("Mission TWO\nFight one begins.......\n");
        else
            return;
        Fight.fightUntilDead(myHero,  new Goblin("The Goblin"));
        if (myHero.gettAlive())
            System.out.println("End of fight one\n");
        else
            return;
        if (myHero.gettAlive())
            System.out.println("Mission TWO\nFight two begins.......\n");
        else
            return;
       Fight.fightUntilDead(myHero, new Nacromancer("The Nacromancer"));
        if (myHero.gettAlive())
            System.out.println("End of fight two\n");
        else
            return;
        if (myHero.gettAlive())
            System.out.println("Mission TWO\nFight three begins.......\n");
        else
            return;
        Fight.fightUntilDead(myHero, new Devil("The Devil"));
        if (myHero.gettAlive())
            System.out.println("End of fight three\n");
        else
            return;
        if (myHero.gettAlive())
            System.out.println("Mission TWO\nFight four begins.......\n");
        else
            return;
        Fight.fightUntilDead(myHero, new Devil("The Devil2"));
        if (myHero.gettAlive())
            System.out.println("End of fight four\n");
        else
            return;
        if (myHero.gettAlive())
            System.out.println("End of fight four and end of Mission TWO\n");
    }
}