package rpg.Characters;

import rpg.Artifacts.Bow;

/**
 * Created by aniov on 2/23/2016.
 */
public class Elf extends Hero{

    public Elf() {
        Bow bow = new Bow(2,1);
        this.setHealth(80 + bow.getArtifactHealth());
        setLevel(1);
    }
}
