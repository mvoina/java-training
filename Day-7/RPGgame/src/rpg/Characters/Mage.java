package rpg.Characters;

import rpg.Artifacts.Helm;

/**
 * Created by aniov on 2/23/2016.
 */
public class Mage extends Hero{

    public Mage() {
        Helm helm = new Helm(4,3);
        this.setHealth(99 + helm.getArtifactHealth());
        setLevel(1);
    }
}
