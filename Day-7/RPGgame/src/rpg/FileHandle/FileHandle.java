package rpg.FileHandle;

import com.sun.org.apache.xerces.internal.xs.StringList;
import rpg.Characters.Hero;

import java.io.*;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

/**
 * Created by aniov on 2/26/2016.
 */
public class FileHandle {

    Scanner f;
    int artifactHealth, artifactPower;
    public boolean openFile(String fileName){

        try{
            f = new Scanner(new File(fileName));
        }
        catch(Exception e){
            System.out.println("File "+fileName+" cannot be opened!!\n");
            return (false);
        }
        return (true);
    }

    public void createPlayerFile(String fileName){

        try{
            new Formatter(fileName + ".txt");
            System.out.println("Player file "+fileName+".txt was created");
        }catch (Exception e){
            System.out.println("New Payer file cannot be created\n");
        }
    }
    public boolean readArtifacts(String artifactName){

        if (!openFile("artifacts.txt"))
            return false;
        while(f.hasNext()){

            if (artifactName.equals(f.next())) {
                artifactHealth = f.nextInt();
                artifactPower = f.nextInt();
                return true;
            }
            else {
                f.nextInt();f.nextInt();
            }
        }
        f.close();
        return false;
    }
    public int getArtifactHealth(){
        return artifactHealth;
    }
    public int getArtifactPower(){
        return artifactPower;
    }
    public void savePlayerStatus(Hero MyHero){

        String playerName = MyHero.getPlayerName();
        if (!openFile(playerName))
        {
            File f = new File(playerName + ".txt");
            System.out.println("File "+playerName+" will be created...\n");
            try{
                PrintWriter output = new PrintWriter(f);
                output.print(playerName + " ");
                output.print(MyHero.getHealth() + " ");
                output.print(MyHero.getAttackPower() + " ");
                output.close();
            } catch (FileNotFoundException e) {
                System.out.println("File " + playerName + " cannot be created\n");
            }
        }
    }
    public boolean confirmPlayerExists(String player){

        if (!openFile("Players.txt"))
            return false;
        while(f.hasNext()){
            String playerName = f.next();
            if ((playerName).equals(player)) {
                System.out.println("Player " + playerName + " was found");
                f.close();
                return true;
            }
        }
        f.close();
        return false;
    }
    public String readPlayerNameFromInput(){

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter Player Name: \n");
        try{
            return (input.readLine());
        }catch (Exception e){
            System.out.println("Player name is in wrong format: \n");
            return null;
        }
    }
    public List<String> readPlayerNameFromFile() {

        if (!openFile("Players.txt"))
            return null;
        List<String> players = new ArrayList<String>();
        while (f.hasNext()) {
            players.add(f.next());
        }
        f.close();
        return players;
    }
    public class player {

        String playerHeroName;
        String playerHeroType;
        List<String> playerHeroArtifactList = new ArrayList<String>();

        public String getPlayerHeroName(){
            return playerHeroName;
        }
        public String getPlayerHeroType(){
            return playerHeroType;
        }
        public List<String> getPlayerHeroArtifactList(){
            return playerHeroArtifactList;
        }
    }
    public List<player> readPlayerChars(String playerName){

        if (openFile(playerName+".txt")){

            List<String> line = new ArrayList<String>();
            while(f.hasNextLine()) {
                line.add(f.nextLine());
            }
            f.close();
            List<player> playerStuff = new ArrayList<player>();
            for(int i = 0; i < line.size(); i++) {

                player pl = new player();
                String[] t = line.get(i).split(" ");
                pl.playerHeroName = t[0];
                pl.playerHeroType = t[1];
                int j = 2;
                while (j < t.length){
                    pl.playerHeroArtifactList.add(t[j]);
                    j++;
                }
                playerStuff.add(pl);
            }
            return playerStuff;
        }
        else{
            return null;
        }
    }
}
