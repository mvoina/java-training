package rpg;

import rpg.FileHandle.FileHandle;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marius on 2/26/2016.
 */
public class StartGame {


    public StartGame() {

//         FileHandle fHandle = new FileHandle();

//        String playerName = fHandle.readPlayerNameFromInput();
//        if (!fHandle.confirmPlayerExists(playerName)) {
//            fHandle.createPlayerFile(playerName);
//        }

//        fHandle.readPlayerChars("Nick");
    }
    public static List<String> getPlayersName(){
        FileHandle fHandle = new FileHandle();
        return fHandle.readPlayerNameFromFile();
    }
    public static List<String> getPlayerHeroesName(String playerName){
        FileHandle fHandle = new FileHandle();

        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        List<String> playerCharNames = new ArrayList<String>();
        for(FileHandle.player temp : playerStuff){
            playerCharNames.add(temp.getPlayerHeroName());
        }
        return  playerCharNames;
    }

    public static String getPlayerHeroType(String playerName, String playerHeroName){
        FileHandle fHandle = new FileHandle();
        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        for(FileHandle.player temp : playerStuff){
            if (temp.getPlayerHeroName().equals(playerHeroName)) {
                return temp.getPlayerHeroType();
            }
        }
        return  null;
    }

    public static ArrayList getPlayersHeroArtifacts(String playerName, String playerHeroName){
        FileHandle fHandle = new FileHandle();
        ArrayList artiListWithStatus = new ArrayList();
        List<FileHandle.player> playerStuff = fHandle.readPlayerChars(playerName);
        List<String> tmp = new ArrayList<>();
        for(FileHandle.player temp : playerStuff) {
            if (temp.getPlayerHeroName().equals(playerHeroName)) {
                tmp = temp.getPlayerHeroArtifactList();
            }
        }
        for(int i = 0; i < tmp.size(); i++) {
            if (fHandle.readArtifacts(tmp.get(i))) {
                artiListWithStatus.add(tmp.get(i) + " " + fHandle.getArtifactHealth() + " " + fHandle.getArtifactPower() + " ");
              }
        }
       return artiListWithStatus;
    }

}
